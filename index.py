from janome.tokenizer import Tokenizer
import pprint
import random
from flask import Flask, render_template
app = Flask(__name__)

lyrics = '''
歌詞を入力してください
'''

@app.route("/")
def hello():
    ado_string = "".join(create_ado(4))
    return render_template('index.html', ado_string=ado_string)

tokenizer = Tokenizer()

def print_token():
    for token in tokenizer.tokenize(lyrics):
        print("    " + str(token))

def create_pos_key(token):
    pos_list = token.part_of_speech.split(',')
    pos_key = f'{pos_list[0]}_{pos_list[1]}'
    return pos_key

def create_part_of_speeches():
    tokens = {}
    for token in tokenizer.tokenize(lyrics):
        pos_key = create_pos_key(token)
        if not pos_key in tokens:
            tokens[pos_key] = {}
        if not token.infl_form in tokens[pos_key]:
            tokens[pos_key][token.infl_form] = []

    return tokens

def scrapeV1(tokens):
    new_tokens = tokens
    for token in tokenizer.tokenize(lyrics):
        pos_key = create_pos_key(token)
        if not token.surface in new_tokens[pos_key][token.infl_form]:
            new_tokens[pos_key][token.infl_form].append(token.surface)

    return new_tokens

def generator_v1(tokens):
    print(get_value(tokens, '名詞_一般', '*'), get_value(tokens, '助詞_係助詞', '*'), get_value(tokens, '動詞_自立', '基本形'))
    return [get_value(tokens, '名詞_一般', '*'), get_value(tokens, '助詞_係助詞', '*'), get_value(tokens, '動詞_自立', '基本形')]

def generator_v2(tokens):
    print(get_value(tokens, '名詞_一般', '*'), get_value(tokens, '助詞_係助詞', '*'), get_value(tokens, '動詞_自立', '未然形'), 'ない')
    return [get_value(tokens, '名詞_一般', '*'), get_value(tokens, '助詞_係助詞', '*'), get_value(tokens, '動詞_自立', '未然形'), 'ない']

def generator_v3(tokens): # 一切合切凡庸な
    print(get_value(tokens, '名詞_副詞可能', '*'), get_value(tokens, '名詞_形容動詞語幹', '*'), get_value(tokens, '助動詞_*', '体言接続'))
    return [get_value(tokens, '名詞_副詞可能', '*'), get_value(tokens, '名詞_形容動詞語幹', '*'), get_value(tokens, '助動詞_*', '体言接続')]

def generator_v4(tokens): # 入社しワーク
    print(get_value(tokens, '名詞_サ変接続', '*'), get_value(tokens, '動詞_自立', '連用形'), get_value(tokens, '名詞_一般', '*'))
    return [get_value(tokens, '名詞_サ変接続', '*'), get_value(tokens, '動詞_自立', '連用形'), get_value(tokens, '名詞_一般', '*')]

def generator_v5(tokens): # あなたが思うより健康です
    print(get_value(tokens, '名詞_代名詞', '*'), get_value(tokens, '助詞_格助詞', '*'), get_value(tokens, '動詞_自立', '基本形'), get_value(tokens, '助詞_格助詞', '*'), get_value(tokens, '名詞_形容動詞語幹', '*'), get_value(tokens, '助動詞_*', '基本形'))
    return [get_value(tokens, '名詞_代名詞', '*'), get_value(tokens, '助詞_格助詞', '*'), get_value(tokens, '動詞_自立', '基本形'), get_value(tokens, '助詞_格助詞', '*'), get_value(tokens, '名詞_形容動詞語幹', '*'), get_value(tokens, '助動詞_*', '基本形')]

def generator_v6(tokens): # その可もなく不可もないメロディー
    print(get_value(tokens, '連体詞_*', '*'), get_value(tokens, '名詞_一般', '*'), get_value(tokens, '助詞_係助詞', '*'), get_value(tokens, '形容詞_自立', '連用テ接続'), get_value(tokens, '名詞_一般', '*'), get_value(tokens, '助詞_係助詞', '*'), get_value(tokens, '形容詞_自立', '基本形'), get_value(tokens, '名詞_一般', '*'))
    return [get_value(tokens, '連体詞_*', '*'), get_value(tokens, '名詞_一般', '*'), get_value(tokens, '助詞_係助詞', '*'), get_value(tokens, '形容詞_自立', '連用テ接続'), get_value(tokens, '名詞_一般', '*'), get_value(tokens, '助詞_係助詞', '*'), get_value(tokens, '形容詞_自立', '基本形'), get_value(tokens, '名詞_一般', '*')]

def generator_v7(tokens): # 頭の出来が違うので問題はナシ
    print(get_value(tokens, '名詞_一般', '*'), get_value(tokens, '助詞_連体化', '*'), get_value(tokens, '名詞_一般', '*'), get_value(tokens, '助詞_格助詞', '*'), get_value(tokens, '動詞_自立', '基本形'), get_value(tokens, '助詞_接続助詞', '*', ), get_value(tokens, '名詞_ナイ形容詞語幹', '*', ), get_value(tokens, '助詞_係助詞', '*', ), get_value(tokens, '名詞_一般', '*', ))
    return [get_value(tokens, '名詞_一般', '*'), get_value(tokens, '助詞_連体化', '*'), get_value(tokens, '名詞_一般', '*'), get_value(tokens, '助詞_格助詞', '*'), get_value(tokens, '動詞_自立', '基本形'), get_value(tokens, '助詞_接続助詞', '*', ), get_value(tokens, '名詞_ナイ形容詞語幹', '*', ), get_value(tokens, '助詞_係助詞', '*', ), get_value(tokens, '名詞_一般', '*', )]

def get_value(tokens, pos, infl):
    return tokens[pos][infl][random.randrange(len(tokens[pos][infl]))]

def create_ado(type):
    tokens = create_part_of_speeches()
    tokens = scrapeV1(tokens)
    
    if type == 1:
        return generator_v1(tokens)
    elif type == 2:
        return generator_v2(tokens)
    elif type == 3:
        return generator_v3(tokens)
    elif type == 4:
        return generator_v4(tokens)
    elif type == 5:
        return generator_v5(tokens)
    elif type == 6:
        return generator_v6(tokens)
    elif type == 7:
        return generator_v7(tokens)

    return ''


if __name__ == '__main__':
    # print_token()
    app.run()